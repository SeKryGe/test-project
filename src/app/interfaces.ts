export interface Type {
  id: number
  type: string
}

export interface Investor{
  _id: string
  amount: string;
  type:string;
  name: {
    first: string;
    last: string;
  },
  company: string;
  email: string;
  phone: string;
  address: string;
}



